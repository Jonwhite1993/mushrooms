// Dependencies
const express = require("express")
const cors = require("cors")
const path = require("path")

// Server variables
const port = 5555

// App
const app = express()

// Express invokations
app.use(cors())
app.use(express.static('public'));

// Pulling in main app directory
app.get('/', (req, res) => { res.sendFile(path.join(__dirname + '/../public/index.html')) })

// Server Port
app.listen(port, () => {
  console.log(`Hey Listen! ${port}`)
})