class Mushroom {
  constructor(name, location, edible, quantity) {
    this.name = name ,
    this.location = location,
    this.edible = edible,
    this.quantity = quantity
  }
}

// const form = document.getElementById('mushroom-form')
// const formData = new formData([form])

const captureForm = () => {
  let name = document.getElementById('mushroom_name').value
  let location = document.getElementById('mushroom_location').value
  let edible = document.getElementById('mushroom_edibility').value
  let quantity = document.getElementById('mushroom_quantity').value
  
  let newMushroom = new Mushroom(name, location, edible, quantity)

  localStorage.setItem('Mushroom', JSON.stringify(newMushroom))
  document.getElementById("mushroom-form").reset();
}

const getMushroom = localStorage.getItem('Mushroom')

const createMushroomCard = (mushroom) => {
  let parsedShroom = JSON.parse(mushroom)
  console.log(parsedShroom)

  let html = `
  <table>
  <tr>
    <th>Name</th>
    <th>Location</th> 
    <th>Edibility</th>
    <th>Quantity</th>
  </tr>
  <tr>
    <td>${parsedShroom.name}</td> 
    <td>${parsedShroom.location}</td> 
    <td>${parsedShroom.edible}</td> 
    <td>${parsedShroom.quantity}</td> 
  </tr>
  </table>
  `
  console.log(html)
  document.getElementById('mushroom-table').innerHTML = html
}

createMushroomCard(getMushroom)
